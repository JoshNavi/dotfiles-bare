"   __            _         __            _       _
"   \ \  ___  ___| |__   /\ \ \__ ___   _(_)_   _(_)_ __ ___  _ __ ___
"    \ \/ _ \/ __| '_ \ /  \/ / _` \ \ / / \ \ / / | '_ ` _ \| '__/ __|
" /\_/ / (_) \__ \ | | / /\  / (_| |\ V /| |\ V /| | | | | | | | | (__
" \___/ \___/|___/_| |_\_\ \/ \__,_| \_/ |_(_)_/ |_|_| |_| |_|_|  \___|
"
" http://patorjk.com/software/taag/#p=display&f=Ogre&t=JoshNavi.vimrc
"
" Inspired by:
" - https://www.reddit.com/r/vim/wiki/vimrctips


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Source .vimrc after saving .vimrc
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
augroup AutoSourceRC
  " Remove all auto-commands from this groupkln
  autocmd!
  autocmd bufwritepost .vimrc source %
augroup END


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Plugins
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Specify a directory for the plugins
call plug#begin('~/.vim/plugged')

" {{ The Basics }}
  " The 'lightest' status line plugin
  Plug 'itchyny/lightline.vim'

" {{ UX }}
  " Just a nice splash screen
  Plug 'mhinz/vim-startify'

  " Completions
  " Plug 'neoclide/coc.nvim', {'branch': 'release'}

" {{ Syntax Highlighting }}
  Plug 'dag/vim-fish'
  Plug 'pangloss/vim-javascript'
  Plug 'leafgarland/typescript-vim'
  Plug 'peitalin/vim-jsx-typescript'
  Plug 'jparise/vim-graphql'

" {{ Colors }}
  " kolor installed globally at '~/.vim/colors'
  Plug 'sainnhe/sonokai'

" {{ General Goodness }}
  Plug 'tpope/vim-surround'
  Plug 'wellle/targets.vim'
  " Add repeat

" Initialize plugin system
call plug#end()


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => fzf
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Assume it has been installed using Homebrew
" This will add some basic integration with fzf
set rtp+=/usr/local/opt/fzf

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => General Settings
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Enable plugins to handle indents
" Dont need 'set smartindent' with this
filetype plugin indent on

" Relative line numbers
set number relativenumber
" Show current position
set ruler
" Keep the cursor centered
set scrolloff=999
" Show matching brackets when text indicator is over them
set showmatch
" Set utf8 as the standard encoding
set encoding=utf8
" Save on buffer switch
set autowrite
" Allow keeping multiple buffers open
set hidden

" Make backspace work the way it should
set backspace=indent,eol,start
" 1000 lines of history
set history=1000
" Highlight searched term
set hlsearch
set incsearch
" Ignore case when searching
set ignorecase
" When searching, only be sensitive when there's a capital letter
set smartcase
" Enable autoread when a file is changed from the outside
set autoread
" Display all matches when tab complete.
set wildmenu

set nobackup
set nowritebackup
set noswapfile


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Remap Keys
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Mash those home keys for Normal mode
inoremap jk <Esc>
inoremap kj <Esc>

" Make capital Y work like the other capital commands (c, d)
" C c$
" D d$
" Y yy
nnoremap Y y$


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Replace with Register
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
inoremap <C-q> <C-r>0<Esc>
nnoremap S "_diwP


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => File Explorer
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Close to my usual 'open file tree' command (ctrl vs cmd)
map <C-\> :Vexplore<CR><C-w><C-=>


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Yank and Paste to System Clipboard
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Close to my usual 'open file tree' command (ctrl vs cmd)
set clipboard+=unnamed


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Status Line
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" The lightline.vim theme
let g:lightline = {
\  'colorscheme' : 'sonokai',
\  'active': {
\    'right': [
\      [ 'lineinfo' ],
\      [ 'percent' ],
\      [ 'filetype' ],
\    ],
\  },
\}

" Always show statusline
set laststatus=2

" Uncomment to prevent non-normal modes showing in powerline and below powerline.
set noshowmode


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Star Page Config
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:startify_lists = [
        \ { 'type': 'files',     'header': ['   MRU']            },
        \ { 'type': 'dir',       'header': ['   MRU '. getcwd()] },
        \ { 'type': 'sessions',  'header': ['   Sessions']       },
        \ { 'type': 'bookmarks', 'header': ['   Bookmarks']      },
        \ { 'type': 'commands',  'header': ['   Commands']       },
        \ ]


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Text, tab and indent related
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Enable auto indenting
set autoindent
" Use spaces instead of tabs
set expandtab
" Be smart when using tabs
set smarttab
" Set the width of a tab to 4 spaces (1 tab == 4 spaces)
set shiftwidth=2
set tabstop=2


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Mouse Scrolling
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set mouse=nicr


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Splits and Tabbed Files
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set splitbelow splitright

" Better :tm: pane switching
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

nnoremap <C-S> <C-W><C-V>

" Removes pipes | that act as seperators on splits
set fillchars+=vert:\|


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Colors and Theming
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Enable syntax highlighting
syntax enable

" Set dark background mode for syntax highlighting
set background=dark

if (has("termguicolors"))
  set termguicolors
endif
if !has('gui_running')
  set t_Co=256
endif

" Set color scheme
let g:sonokai_style = 'shusia'
let g:sonokai_enable_italic = 1
colorscheme sonokai

" Change curor styles
let &t_SI = "\<esc>[1 q"
let &t_SR = "\<esc>[2 q"
let &t_EI = "\<esc>[0 q"
set cursorline
" highlight CursorLine guibg=Grey20
" TODO: Update search highlight color


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Whitespace
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Show trailing whitespace
set list listchars=tab:\ \ ,trail:·

" Delete trailing whitespaces
function! StripTrailingWhitespace()
  if !&binary && &filetype != 'diff'
    normal mz
    normal Hmy
    %s/\s\+$//e
    normal 'yz<CR>
    normal `z
  endif
endfunction

" Automatically delete trailing whitespaces on file change
augroup AutoStripTrailingWhitespace
  " Remove all auto-commands from this groupkln
  autocmd!
  autocmd BufWritePre * :call StripTrailingWhitespace()
augroup END
