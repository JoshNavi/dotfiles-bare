set -eo pipefail

cp .vimrc ~/.vimrc
cp init.vim ~/.config/nvim/init.vim
cp init.fish ~/.config/omf/init.fish
cp alacritty.yml ~/.config/alacritty/alacritty.yml
cp kitty.conf ~/.config/kitty/kitty.conf
