alias vim 'nvim'
alias grep 'rg'
alias ls 'exa -la --group-directories-first'
alias vimrc 'vim ~/.vimrc'

set -gx FZF_DEFAULT_COMMAND 'rg --files'
set -xg JAVA_HOME /Library/Java/JavaVirtualMachines/amazon-corretto-11.jdk/Contents/Home/

for f in $OMF_CONFIG/themes/roshua/git/*
  source $f
end
